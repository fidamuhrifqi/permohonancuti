package com.springboottest.permohonancuti.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.springboottest.permohonancuti.model.BucketApproval;

@Repository
public interface BucketApprovalRepository extends JpaRepository<BucketApproval, Long>{

}
