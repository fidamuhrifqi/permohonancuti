package com.springboottest.permohonancuti.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.springboottest.permohonancuti.model.Position;

@Repository
public interface PositionRepository  extends JpaRepository<Position, Long>{

}
