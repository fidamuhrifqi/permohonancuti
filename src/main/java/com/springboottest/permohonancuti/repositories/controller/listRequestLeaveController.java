package com.springboottest.permohonancuti.repositories.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springboottest.permohonancuti.model.UserLeaveRequest;
import com.springboottest.permohonancuti.process.dto.ListRequestLeaveDTO;
import com.springboottest.permohonancuti.repositories.UserLeaveRequestRepository;

@RestController
@RequestMapping("/api/listrequestleave")
public class listRequestLeaveController {

	@Autowired
	UserLeaveRequestRepository userLeaveRequestRepository;
	
	 @GetMapping("/get")
	 public Map<String, Object> getListRequestLeave(){
		
		 List<UserLeaveRequest> userLeaveRequestList = userLeaveRequestRepository.findAll();
		 List<ListRequestLeaveDTO> listRequestLeaveDTO = new ArrayList<ListRequestLeaveDTO>();
		 Map<String, Object> result = new HashMap<String,Object>();
		 
		 for (UserLeaveRequest userLeaveRequest : userLeaveRequestList) {
			 ListRequestLeaveDTO requestLeaveDTO = new ListRequestLeaveDTO();
			 
			 	short status = userLeaveRequest.getStatus();
				String namaStatus="";
				if (status == 1) {
					namaStatus = "waiting";
				}
				else if (status == 2) {
					namaStatus = "approved";
				}
				else if (status == 3) {
					namaStatus = "rejected";
				}
			 
			 requestLeaveDTO.setUserId(userLeaveRequest.getUser().getUserId());
			 requestLeaveDTO.setLeaveDateFrom(userLeaveRequest.getLeaveDateFrom());
			 requestLeaveDTO.setLeaveDateTo(userLeaveRequest.getLeaveDateTo());
			 requestLeaveDTO.setDescription(userLeaveRequest.getBucketApproval().getDescription());
			 requestLeaveDTO.setStatus(namaStatus);
			 requestLeaveDTO.setResolverBy(userLeaveRequest.getUpdateBy());
			 requestLeaveDTO.setResolverDate(userLeaveRequest.getUpdateDate());
			 
			 listRequestLeaveDTO.add(requestLeaveDTO);
			 
		}
		 
	        result.put("Response : ", "Success");
	        result.put("Item : ", listRequestLeaveDTO);
	        result.put("Total Item : ", listRequestLeaveDTO.size());
	        
		 return result;
	 }
}
