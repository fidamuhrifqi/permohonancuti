package com.springboottest.permohonancuti.repositories.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springboottest.permohonancuti.model.LeaveQuota;
import com.springboottest.permohonancuti.model.User;
import com.springboottest.permohonancuti.process.dto.UserDetailsDTO;
import com.springboottest.permohonancuti.repositories.LeaveQuotaRepository;
import com.springboottest.permohonancuti.repositories.UserRepository;

@RestController
@RequestMapping("/api/userdetails")
public class UserDetailsController {

	@Autowired
	UserRepository userRepository;
	@Autowired
	LeaveQuotaRepository leaveQuotaRepository;
	
	 @GetMapping("/get")
	 public Map<String, Object> UserDetails(){
		
		 List<User> userList = userRepository.findAll();
		 List<LeaveQuota> leaveQuotaList = leaveQuotaRepository.findAll();
		 List<UserDetailsDTO> listUserDetailsDTO = new ArrayList<UserDetailsDTO>();
		 Map<String, Object> result = new HashMap<String,Object>();
		 
		 for (User user : userList) {
			 UserDetailsDTO	userDetailsDTO = new UserDetailsDTO();
			 
			 userDetailsDTO.setUserId(user.getUserId());
			 userDetailsDTO.setUserName(user.getUserName());
			 userDetailsDTO.setPosition(user.getPosition().getPositionName());
			 userDetailsDTO.setLeaveInOneYear(user.getPosition().getPositionLeave().getTotalPositionLeave() + " hari");
			 String leaveTaken = "";
			 String restOfLeave = "";
				
			 for (LeaveQuota leaveQuota : leaveQuotaList) {
					if (leaveQuota.getUser().getUserId() == user.getUserId()) {
						leaveTaken = leaveQuota.getLeaveTaken() + " hari";
						restOfLeave = leaveQuota.getRestOfLeave() + " hari";		
					}
				}
			 
			 userDetailsDTO.setLeaveTaken(leaveTaken);
			 userDetailsDTO.setRestOfLeave(restOfLeave);
			 
			 listUserDetailsDTO.add(userDetailsDTO);
		}
		 
	        result.put("Response : ", "Success");
	        result.put("Item : ", listUserDetailsDTO);
	        result.put("Total Item : ", listUserDetailsDTO.size());
	        
		 return result;
	 }
}
