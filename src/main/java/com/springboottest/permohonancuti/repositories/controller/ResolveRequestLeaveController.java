package com.springboottest.permohonancuti.repositories.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springboottest.permohonancuti.model.BucketApproval;
import com.springboottest.permohonancuti.model.LeaveQuota;
import com.springboottest.permohonancuti.model.UserLeaveRequest;
import com.springboottest.permohonancuti.process.dto.ResolveRequestLeaveDTO;
import com.springboottest.permohonancuti.repositories.BucketApprovalRepository;
import com.springboottest.permohonancuti.repositories.LeaveQuotaRepository;
import com.springboottest.permohonancuti.repositories.UserLeaveRequestRepository;

@RestController
@RequestMapping("/api/resolverequestleave")
public class ResolveRequestLeaveController {

	@Autowired
	BucketApprovalRepository bucketApprovalRepository;
	@Autowired
	UserLeaveRequestRepository userLeaveRequestRepository;
	@Autowired
	LeaveQuotaRepository leaveQuotaRepository;

	@PutMapping("/update")
	public Map<String, Object> updateRequest(@Valid @RequestBody ResolveRequestLeaveDTO resolveRequestLeaveDTO) throws ParseException{

		long bucketId	= resolveRequestLeaveDTO.getBucketApprovalId();
		String namaStatus = resolveRequestLeaveDTO.getStatus();
		String resolverReason = resolveRequestLeaveDTO.getResolverReason();
		String resolvedBy = resolveRequestLeaveDTO.getResolvedBy();
		Date resolvedDate = new SimpleDateFormat("dd-MM-yyyy").parse(resolveRequestLeaveDTO.getResolvedDate());
		short status = 1;
		String massage = "";
		String response = "";

		if (namaStatus.equalsIgnoreCase("approved")) {
			status = 2;
		}
		else if (namaStatus.equalsIgnoreCase("rejected")) {
			status = 3;
		}

		Optional<BucketApproval> bucketApprovalCheck = bucketApprovalRepository.findById(bucketId);
		if (!bucketApprovalCheck.isPresent()) {
			massage = "Permohonan dengan ID "+ bucketId +" tidak ditemukan.";
			response = "Error : bucketApprovalId tidak ada";
		}
		else {
			BucketApproval bucketApproval = bucketApprovalRepository.getOne(bucketId);
			if (resolvedDate.getTime() < bucketApproval.getCreatedDate().getTime()) {
				massage = "Kesalahan data, tanggal keputusan tidak bisa lebih awal dari pengajuan cuti.";
				response = "Error : resolvedDate < leaveDateApplication";
			}
			else if (bucketApproval.getStatus() == 1) {
				bucketApproval.setStatus(status);
				bucketApproval.setResolverReason(resolverReason);
				bucketApproval.setUpdatedBy(resolvedBy);
				bucketApproval.setUpdatedDate(resolvedDate);
				bucketApprovalRepository.save(bucketApproval);

				List<UserLeaveRequest> userLeaveRequestList = userLeaveRequestRepository.findAll();
				for (UserLeaveRequest userLeaveRequest : userLeaveRequestList) {
					if (userLeaveRequest.getBucketApproval() == bucketApproval) {
						userLeaveRequest.setStatus(status);
						userLeaveRequest.setUpdateBy(resolvedBy);
						userLeaveRequest.setUpdateDate(resolvedDate);
						userLeaveRequestRepository.save(userLeaveRequest);

						if (status == 2) {
							List<LeaveQuota> leaveQuotaList = leaveQuotaRepository.findAll();
							int differenceTime = (int) (userLeaveRequest.getLeaveDateTo().getTime() - userLeaveRequest.getLeaveDateFrom().getTime());
							int lengthOfLeave = (differenceTime / (24 * 60 * 60 * 1000)) + 1;
							
							for (LeaveQuota leaveQuota : leaveQuotaList) {
								if (leaveQuota.getUser() == userLeaveRequest.getUser()) {
									leaveQuota.setLeaveTaken(lengthOfLeave);
									int restOfLeave = leaveQuota.getRestOfLeave();
									leaveQuota.setRestOfLeave(restOfLeave - lengthOfLeave);
								}

							}
						}
					}
				}
				massage = "Permohonan dengan ID "+ bucketId +" telah berhasil diputuskan.";
				response = "Success";
			}
			else if (bucketApproval.getStatus() != 1) {
				if (status == 2) {
					namaStatus = "approv";
				}
				else if (status == 3) {
					namaStatus = "reject";
				}
				massage = "Permohonan dengan ID "+ bucketId +" telah ditindak lanjuti";
				response = "Error : permohonan telah di " + namaStatus;
			}
		}

		Map<String, Object> result = new HashMap<String,Object>();

		result.put("Response : ", response);
		result.put("Massage : ", massage);

		return result;
	}
}
