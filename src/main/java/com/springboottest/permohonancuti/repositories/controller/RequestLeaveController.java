package com.springboottest.permohonancuti.repositories.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springboottest.permohonancuti.model.BucketApproval;
import com.springboottest.permohonancuti.model.LeaveQuota;
import com.springboottest.permohonancuti.model.User;
import com.springboottest.permohonancuti.model.UserLeaveRequest;
import com.springboottest.permohonancuti.process.dto.RequestLeaveDTO;
import com.springboottest.permohonancuti.repositories.BucketApprovalRepository;
import com.springboottest.permohonancuti.repositories.LeaveQuotaRepository;
import com.springboottest.permohonancuti.repositories.UserLeaveRequestRepository;
import com.springboottest.permohonancuti.repositories.UserRepository;

@RestController
@RequestMapping("/api/requestleave")
public class RequestLeaveController {

	@Autowired
	UserRepository userRepository;
	@Autowired
	BucketApprovalRepository bucketApprovalRepository;
	@Autowired
	UserLeaveRequestRepository userLeaveRequestRepository;
	@Autowired
	LeaveQuotaRepository leaveQuotaRepository;
	
	@PostMapping("/create")
	public Map<String, Object> RequestLeaverDTO(@Valid @RequestBody RequestLeaveDTO requestLeaveDTO) throws ParseException{
		 List<User> userList = userRepository.findAll();
			List<LeaveQuota> leaveQuotaList = leaveQuotaRepository.findAll();
			int restOfLeave = 0;
			User userObject = new User();
			Long userId = requestLeaveDTO.getUserId();
			String user = "";
			String description = requestLeaveDTO.getDescription();
			Date leaveFrom = new SimpleDateFormat("dd-MM-yyyy").parse(requestLeaveDTO.getLeaveDateFrom());
			Date leaveTo = new SimpleDateFormat("dd-MM-yyyy").parse(requestLeaveDTO.getLeaveDateTo());
			int differenceTime = (int) (leaveTo.getTime() - leaveFrom.getTime());
			int lengthOfLeave = (differenceTime / (24 * 60 * 60 * 1000)) + 1;
			String massage = "";
			String response = "";
			Map<String, Object> result = new HashMap<String,Object>();
			
			
			for (User userFromList : userList) {
				if (userFromList.getUserId() == userId) {
					user = userFromList.getUserName();
					userObject = userFromList;
				}
			}
			
			for (LeaveQuota leaveQuota : leaveQuotaList) {
				if (leaveQuota.getUser() == userObject) {
					restOfLeave = leaveQuota.getRestOfLeave();
				}
			}

			if (restOfLeave < 1) {
				response = "Error : Jatah Cuti Habis";
				massage =  "Mohon maaf, jatah cuti Anda telah habis.";

			}
			else if (leaveFrom.getTime() > leaveTo.getTime()) {
				response = "Error : Tanggal Salah, leaveDateFrom > leaveDateTo";
				massage =  "Tanggal yang Anda ajukan tidak valid.";
			}
			else if (leaveFrom.getTime() < new Date().getTime()) {
				response = "Error : Cuti Backdate, tanggal pengajuan cuti < tanggal hari ini";
				massage = "Tanggal yang Anda ajukan telah lampau, silahkan ganti tanggal pengajuan cuti anda.";
			}
			else if (lengthOfLeave > restOfLeave) {
				response = "Error : Jatah Cuti Tidak Cukup";
				massage =  "Mohon maaf, jatah cuti Anda tidak cukup untuk digunakan dari tanggal "+ requestLeaveDTO.getLeaveDateFrom() +" sampai "+requestLeaveDTO.getLeaveDateTo()+" ("+ lengthOfLeave + "hari). Jatah cuti Anda yang tersisa adalah "+ restOfLeave +" hari.";

			}
			else {
				BucketApproval bucketApproval = new BucketApproval();
				bucketApproval.setApplicant(user);
				bucketApproval.setDescription(description);
				bucketApproval.setStatus((short)1);
				bucketApproval.setCreatedBy(user);
				bucketApproval.setCreatedDate(new Date());
				bucketApprovalRepository.save(bucketApproval);
				
				UserLeaveRequest userLeaveRequest = new UserLeaveRequest();
				userLeaveRequest.setBucketApproval(bucketApproval);
				userLeaveRequest.setUser(userObject);
				userLeaveRequest.setDate(new Date());
				userLeaveRequest.setLeaveDateFrom(leaveFrom);
				userLeaveRequest.setLeaveDateTo(leaveTo);
				userLeaveRequest.setRestOfLeave(restOfLeave);
				userLeaveRequest.setStatus((short)1);
				userLeaveRequest.setCreatedBy(user);
				bucketApproval.setCreatedDate(new Date());
				userLeaveRequestRepository.save(userLeaveRequest);
				
				response = "Success";
				massage =  "Permohonan Anda sedang diproses.";
			}
		 
	        result.put("Response : ", response);
	        result.put("Massage : ", massage);
	        
		 return result;
	 }

}
