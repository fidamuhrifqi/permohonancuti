package com.springboottest.permohonancuti.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.springboottest.permohonancuti.model.LeaveQuota;

@Repository
public interface LeaveQuotaRepository  extends JpaRepository<LeaveQuota, Long>{

}
