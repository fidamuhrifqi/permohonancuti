package com.springboottest.permohonancuti.model;
// Generated Jan 20, 2020 4:55:21 PM by Hibernate Tools 5.1.10.Final

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * LeaveQuota generated by hbm2java
 */
@Entity
@Table(name = "leave_quota", schema = "public")
public class LeaveQuota implements java.io.Serializable {

	private long leaveQuotaId;
	private PositionLeave positionLeave;
	private User user;
	private Integer leaveTaken;
	private Integer restOfLeave;

	public LeaveQuota() {
	}

	public LeaveQuota(long leaveQuotaId, PositionLeave positionLeave, User user) {
		this.leaveQuotaId = leaveQuotaId;
		this.positionLeave = positionLeave;
		this.user = user;
	}

	public LeaveQuota(long leaveQuotaId, PositionLeave positionLeave, User user, Integer leaveTaken,
			Integer restOfLeave) {
		this.leaveQuotaId = leaveQuotaId;
		this.positionLeave = positionLeave;
		this.user = user;
		this.leaveTaken = leaveTaken;
		this.restOfLeave = restOfLeave;
	}

	@Id
	@Column(name = "leave_quota_id", unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "generator_leave_quota_id_seq")
	@SequenceGenerator(name="generator_leave_quota_id_seq", sequenceName="leave_quota_id_seq", schema = "public", allocationSize = 1)
	public long getLeaveQuotaId() {
		return this.leaveQuotaId;
	}

	public void setLeaveQuotaId(long leaveQuotaId) {
		this.leaveQuotaId = leaveQuotaId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "position_leave_id", nullable = false)
	public PositionLeave getPositionLeave() {
		return this.positionLeave;
	}

	public void setPositionLeave(PositionLeave positionLeave) {
		this.positionLeave = positionLeave;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id", nullable = false)
	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Column(name = "leave_taken")
	public Integer getLeaveTaken() {
		return this.leaveTaken;
	}

	public void setLeaveTaken(Integer leaveTaken) {
		this.leaveTaken = leaveTaken;
	}

	@Column(name = "rest_of_leave")
	public Integer getRestOfLeave() {
		return this.restOfLeave;
	}

	public void setRestOfLeave(Integer restOfLeave) {
		this.restOfLeave = restOfLeave;
	}

}
