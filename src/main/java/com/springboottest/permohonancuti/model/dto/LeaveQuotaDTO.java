package com.springboottest.permohonancuti.model.dto;

import com.springboottest.permohonancuti.model.User;

public class LeaveQuotaDTO {

	private long leaveQuotaId;
	private PositionLeaveDTO positionLeave;
	private User user;
	private Integer leaveTaken;
	private Integer restOfLeave;
	
	public long getLeaveQuotaId() {
		return leaveQuotaId;
	}
	public void setLeaveQuotaId(long leaveQuotaId) {
		this.leaveQuotaId = leaveQuotaId;
	}
	public PositionLeaveDTO getPositionLeave() {
		return positionLeave;
	}
	public void setPositionLeave(PositionLeaveDTO positionLeave) {
		this.positionLeave = positionLeave;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Integer getLeaveTaken() {
		return leaveTaken;
	}
	public void setLeaveTaken(Integer leaveTaken) {
		this.leaveTaken = leaveTaken;
	}
	public Integer getRestOfLeave() {
		return restOfLeave;
	}
	public void setRestOfLeave(Integer restOfLeave) {
		this.restOfLeave = restOfLeave;
	}
	
	
}
