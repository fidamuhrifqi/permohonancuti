package com.springboottest.permohonancuti.model.dto;

import java.util.Date;

public class PositionLeaveDTO {
	
	private long positionLeaveId;
	private int totalPositionLeave;
	private String createdBy;
	private Date createdDate;
	private String updateBy;
	private Date updateDate;
	
	public long getPositionLeaveId() {
		return positionLeaveId;
	}
	public void setPositionLeaveId(long positionLeaveId) {
		this.positionLeaveId = positionLeaveId;
	}
	public int getTotalPositionLeave() {
		return totalPositionLeave;
	}
	public void setTotalPositionLeave(int totalPositionLeave) {
		this.totalPositionLeave = totalPositionLeave;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getUpdateBy() {
		return updateBy;
	}
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
}
