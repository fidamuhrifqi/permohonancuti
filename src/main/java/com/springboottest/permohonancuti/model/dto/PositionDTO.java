package com.springboottest.permohonancuti.model.dto;

import java.util.Date;

public class PositionDTO {

	private long positionId;
	private PositionLeaveDTO positionLeave;
	private String positionName;
	private String createdBy;
	private Date createdDate;
	private String updateBy;
	private Date updateDate;
	
	public long getPositionId() {
		return positionId;
	}
	public void setPositionId(long positionId) {
		this.positionId = positionId;
	}
	public PositionLeaveDTO getPositionLeave() {
		return positionLeave;
	}
	public void setPositionLeave(PositionLeaveDTO positionLeave) {
		this.positionLeave = positionLeave;
	}
	public String getPositionName() {
		return positionName;
	}
	public void setPositionName(String positionName) {
		this.positionName = positionName;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getUpdateBy() {
		return updateBy;
	}
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
}
