package com.springboottest.permohonancuti.model.dto;

import java.util.Date;

import com.springboottest.permohonancuti.model.User;

public class UserLeaveRequestDTO {

	private long userLeaveRequestId;
	private BucketApprovalDTO bucketApproval;
	private User user;
	private Date date;
	private Date leaveDateFrom;
	private Date leaveDateTo;
	private Integer restOfLeave;
	private String createdBy;
	private Date createdDate;
	private String updateBy;
	private Date updateDate;
	public long getUserLeaveRequestId() {
		return userLeaveRequestId;
	}
	public void setUserLeaveRequestId(long userLeaveRequestId) {
		this.userLeaveRequestId = userLeaveRequestId;
	}
	public BucketApprovalDTO getBucketApproval() {
		return bucketApproval;
	}
	public void setBucketApproval(BucketApprovalDTO bucketApproval) {
		this.bucketApproval = bucketApproval;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Date getLeaveDateFrom() {
		return leaveDateFrom;
	}
	public void setLeaveDateFrom(Date leaveDateFrom) {
		this.leaveDateFrom = leaveDateFrom;
	}
	public Date getLeaveDateTo() {
		return leaveDateTo;
	}
	public void setLeaveDateTo(Date leaveDateTo) {
		this.leaveDateTo = leaveDateTo;
	}
	public Integer getRestOfLeave() {
		return restOfLeave;
	}
	public void setRestOfLeave(Integer restOfLeave) {
		this.restOfLeave = restOfLeave;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getUpdateBy() {
		return updateBy;
	}
	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
}
