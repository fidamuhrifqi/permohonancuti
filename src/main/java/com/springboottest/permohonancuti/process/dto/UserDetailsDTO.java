package com.springboottest.permohonancuti.process.dto;

public class UserDetailsDTO {

	private long userId;
	private String position;
	private String userName;
	private String leaveInOneYear;
	private String leaveTaken;
	private String restOfLeave;
	
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getLeaveInOneYear() {
		return leaveInOneYear;
	}
	public void setLeaveInOneYear(String leaveInOneYear) {
		this.leaveInOneYear = leaveInOneYear;
	}
	public String getLeaveTaken() {
		return leaveTaken;
	}
	public void setLeaveTaken(String leaveTaken) {
		this.leaveTaken = leaveTaken;
	}
	public String getRestOfLeave() {
		return restOfLeave;
	}
	public void setRestOfLeave(String restOfLeave) {
		this.restOfLeave = restOfLeave;
	}
}
