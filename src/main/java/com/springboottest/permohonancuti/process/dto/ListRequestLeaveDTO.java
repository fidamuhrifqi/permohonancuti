package com.springboottest.permohonancuti.process.dto;

import java.util.Date;

public class ListRequestLeaveDTO {

	private long userId;
	private Date leaveDateFrom;
	private Date leaveDateTo;
	private String description;
	private String Status;
	private String resolverReason;
	private String resolverBy;
	private Date resolverDate;
	
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public Date getLeaveDateFrom() {
		return leaveDateFrom;
	}
	public void setLeaveDateFrom(Date leaveDateFrom) {
		this.leaveDateFrom = leaveDateFrom;
	}
	public Date getLeaveDateTo() {
		return leaveDateTo;
	}
	public void setLeaveDateTo(Date leaveDateTo) {
		this.leaveDateTo = leaveDateTo;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getStatus() {
		return Status;
	}
	public void setStatus(String status) {
		Status = status;
	}
	public String getResolverReason() {
		return resolverReason;
	}
	public void setResolverReason(String resolverReason) {
		this.resolverReason = resolverReason;
	}
	public String getResolverBy() {
		return resolverBy;
	}
	public void setResolverBy(String resolverBy) {
		this.resolverBy = resolverBy;
	}
	public Date getResolverDate() {
		return resolverDate;
	}
	public void setResolverDate(Date resolverDate) {
		this.resolverDate = resolverDate;
	}
}