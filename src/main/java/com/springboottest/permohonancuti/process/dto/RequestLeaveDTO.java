package com.springboottest.permohonancuti.process.dto;

import java.util.Date;

public class RequestLeaveDTO {
	private long userId;
	private String leaveDateFrom;
	private String leaveDateTo;
	private String description;
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public String getLeaveDateFrom() {
		return leaveDateFrom;
	}
	public void setLeaveDateFrom(String leaveDateFrom) {
		this.leaveDateFrom = leaveDateFrom;
	}
	public String getLeaveDateTo() {
		return leaveDateTo;
	}
	public void setLeaveDateTo(String leaveDateTo) {
		this.leaveDateTo = leaveDateTo;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}
