package com.springboottest.permohonancuti.controller;

import java.util.Date;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.io.iona.springboot.actionflows.custom.CustomOnInsert;
import com.io.iona.springboot.actionflows.custom.CustomOnUpdate;
import com.io.iona.springboot.controllers.HibernateCRUDController;
import com.io.iona.springboot.sources.HibernateDataSource;
import com.io.iona.springboot.sources.HibernateDataUtility;
import com.springboottest.permohonancuti.model.LeaveQuota;
import com.springboottest.permohonancuti.model.Position;
import com.springboottest.permohonancuti.model.PositionLeave;
import com.springboottest.permohonancuti.model.User;
import com.springboottest.permohonancuti.model.dto.UserDTO;
import com.springboottest.permohonancuti.repositories.LeaveQuotaRepository;
import com.springboottest.permohonancuti.repositories.PositionLeaveRepository;
import com.springboottest.permohonancuti.repositories.PositionRepository;
import com.springboottest.permohonancuti.repositories.UserRepository;

@RestController
@RequestMapping("/api/user")
public class UserController  extends HibernateCRUDController<User, UserDTO> implements CustomOnInsert<User, UserDTO>,CustomOnUpdate<User, UserDTO>{

	@Autowired
	UserRepository userRepository;
	@Autowired
	PositionLeaveRepository positionLeaveRepository;
	@Autowired 
	LeaveQuotaRepository leaveQuotaRepository;
	@Autowired
	PositionRepository positionRepository;
	
	ModelMapper modelMapper = new ModelMapper();

	@Override
	public User onInsert(HibernateDataUtility dataUtility, HibernateDataSource<User, UserDTO> dataSource) throws Exception {

		String userName = dataSource.getDataTransferObject().getUserName();
		String createdBy = dataSource.getDataTransferObject().getCreatedBy();
		long positioId =  dataSource.getDataTransferObject().getPosition().getPositionId();
		List<Position> positionList = positionRepository.findAll();
		Position positionUser = new Position();
		PositionLeave positionLeave = new PositionLeave();
		for (Position position : positionList) {
			if (position.getPositionId() == positioId) {
				positionLeave = position.getPositionLeave();
				positionUser = position;
			}
		}
		
		UserDTO userDTO = new UserDTO(userName, createdBy);
		User user = modelMapper.map(userDTO, User.class);
		user.setPosition(positionUser);
		userRepository.save(user);
			
		LeaveQuota leaveQuota = new LeaveQuota();
		leaveQuota.setUser(user);
		leaveQuota.setPositionLeave(positionLeave);
		leaveQuota.setLeaveTaken(0);
		leaveQuota.setRestOfLeave(positionLeave.getTotalPositionLeave());
		leaveQuotaRepository.save(leaveQuota);
		
		return user;
	}

	@Override
	public User onUpdate(HibernateDataUtility dataUtility, HibernateDataSource<User, UserDTO> dataSource) throws Exception {

		long userId = dataSource.getDataTransferObject().getUserId();
		UserDTO userDTO = modelMapper.map(userRepository.getOne(userId), UserDTO.class);
		userDTO.setUserName(dataSource.getDataTransferObject().getUserName());
		userDTO.setPosition(dataSource.getDataTransferObject().getPosition());	
		userDTO.setCreatedBy(userRepository.getOne(dataSource.getDataTransferObject().getUserId()).getCreatedBy());
		userDTO.setCreatedDate(userRepository.getOne(dataSource.getDataTransferObject().getUserId()).getCreatedDate());
		userDTO.setUpdatedBy(dataSource.getDataTransferObject().getUpdatedBy());
		userDTO.setUpdatedDate(new Date());
		User user = modelMapper.map(userDTO, User.class);
		userRepository.save(user);
		
		List<LeaveQuota> leaveQuotaList = leaveQuotaRepository.findAll();
		for (LeaveQuota leaveQuota : leaveQuotaList) {
			if (leaveQuota.getUser().getUserId() == userId) {
				leaveQuota.setPositionLeave(user.getPosition().getPositionLeave());
				leaveQuotaRepository.save(leaveQuota);
			}
		}
		return user;
	}

}
