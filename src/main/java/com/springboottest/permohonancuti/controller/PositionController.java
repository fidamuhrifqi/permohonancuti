package com.springboottest.permohonancuti.controller;

import java.util.Date;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.io.iona.springboot.actionflows.custom.CustomOnInsert;
import com.io.iona.springboot.actionflows.custom.CustomOnUpdate;
import com.io.iona.springboot.controllers.HibernateCRUDController;
import com.io.iona.springboot.sources.HibernateDataSource;
import com.io.iona.springboot.sources.HibernateDataUtility;
import com.springboottest.permohonancuti.model.Position;
import com.springboottest.permohonancuti.model.PositionLeave;
import com.springboottest.permohonancuti.model.dto.PositionDTO;
import com.springboottest.permohonancuti.repositories.PositionLeaveRepository;
import com.springboottest.permohonancuti.repositories.PositionRepository;

@RestController
@RequestMapping("/api/position")
public class PositionController extends HibernateCRUDController<Position, PositionDTO> implements CustomOnInsert<Position, PositionDTO>,CustomOnUpdate<Position, PositionDTO>{

	@Autowired
	PositionRepository positionRepository;
	@Autowired
	PositionLeaveRepository PositionLeaveRepository;

	ModelMapper modelMapper = new ModelMapper();

	@Override
	public Position onInsert(HibernateDataUtility dataUtility, HibernateDataSource<Position, PositionDTO> dataSource)
			throws Exception {

		List<PositionLeave> positionLeaveList = PositionLeaveRepository.findAll();
		PositionLeave positionLeaveFix = new PositionLeave();
		for (PositionLeave positionLeave : positionLeaveList) {
			if (positionLeave.getPositionLeaveId() == dataSource.getDataTransferObject().getPositionLeave().getPositionLeaveId()) {
				positionLeaveFix = positionLeave;
			}
		}

		Position position = new Position();
		position.setPositionName(dataSource.getDataTransferObject().getPositionName());
		position.setCreatedBy(dataSource.getDataTransferObject().getCreatedBy());
		position.setPositionLeave(positionLeaveFix);
		position.setCreatedDate(new Date());
		positionRepository.save(position);

		return position;
	}

	@Override
	public Position onUpdate(HibernateDataUtility dataUtility, HibernateDataSource<Position, PositionDTO> dataSource)
			throws Exception {

		long positionId = dataSource.getDataTransferObject().getPositionId();
		PositionDTO positionDTO = modelMapper.map(positionRepository.getOne(positionId), PositionDTO.class);
		positionDTO.setPositionName(dataSource.getDataTransferObject().getPositionName());
		positionDTO.setUpdateBy(dataSource.getDataTransferObject().getUpdateBy());
		positionDTO.setPositionLeave(dataSource.getDataTransferObject().getPositionLeave());
		positionDTO.setUpdateDate(new Date());
		Position position = modelMapper.map(positionDTO, Position.class);
		positionRepository.save(position);

		return position;
	}

}
