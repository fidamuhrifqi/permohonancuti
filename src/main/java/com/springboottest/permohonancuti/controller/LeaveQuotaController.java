package com.springboottest.permohonancuti.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.io.iona.springboot.controllers.HibernateCRUDController;
import com.springboottest.permohonancuti.model.LeaveQuota;
import com.springboottest.permohonancuti.model.dto.LeaveQuotaDTO;

@RestController
@RequestMapping("/api/leavequota")
public class LeaveQuotaController extends HibernateCRUDController<LeaveQuota, LeaveQuotaDTO>{

}
