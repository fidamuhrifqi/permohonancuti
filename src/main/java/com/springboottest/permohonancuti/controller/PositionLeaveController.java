package com.springboottest.permohonancuti.controller;

import java.util.Date;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.io.iona.springboot.actionflows.custom.CustomOnInsert;
import com.io.iona.springboot.actionflows.custom.CustomOnUpdate;
import com.io.iona.springboot.controllers.HibernateCRUDController;
import com.io.iona.springboot.sources.HibernateDataSource;
import com.io.iona.springboot.sources.HibernateDataUtility;
import com.springboottest.permohonancuti.model.PositionLeave;
import com.springboottest.permohonancuti.model.dto.PositionLeaveDTO;
import com.springboottest.permohonancuti.repositories.PositionLeaveRepository;

@RestController
@RequestMapping("/api/positionleave")
public class PositionLeaveController extends HibernateCRUDController<PositionLeave, PositionLeaveDTO> implements CustomOnInsert<PositionLeave, PositionLeaveDTO>,CustomOnUpdate<PositionLeave, PositionLeaveDTO>{

	@Autowired
	PositionLeaveRepository PositionLeaveRepository;
	
	ModelMapper modelMapper = new ModelMapper();

	@Override
	public PositionLeave onInsert(HibernateDataUtility dataUtility, HibernateDataSource<PositionLeave, PositionLeaveDTO> dataSource)
			throws Exception {
		PositionLeave positionLeave = new PositionLeave();
		positionLeave.setTotalPositionLeave(dataSource.getDataTransferObject().getTotalPositionLeave());
		positionLeave.setCreatedBy(dataSource.getDataTransferObject().getCreatedBy());
		positionLeave.setCreatedDate(new Date());
		PositionLeaveRepository.save(positionLeave);

		return positionLeave;
	}

	@Override
	public PositionLeave onUpdate(HibernateDataUtility dataUtility, HibernateDataSource<PositionLeave, PositionLeaveDTO> dataSource)
			throws Exception {
		
		long positionLeaveId = dataSource.getDataTransferObject().getPositionLeaveId();
		PositionLeaveDTO positionLeaveDTO = modelMapper.map(PositionLeaveRepository.getOne(positionLeaveId), PositionLeaveDTO.class);
		positionLeaveDTO.setTotalPositionLeave(dataSource.getDataTransferObject().getTotalPositionLeave());
		positionLeaveDTO.setUpdateBy(dataSource.getDataTransferObject().getUpdateBy());
		positionLeaveDTO.setUpdateDate(new Date());
		PositionLeave positionLeave = modelMapper.map(positionLeaveDTO, PositionLeave.class);
		PositionLeaveRepository.save(positionLeave);

		return positionLeave;
	}
}
